package com.infobip.urlshortener.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class AccountRequest {

    @Getter
    private String accountId;

}
