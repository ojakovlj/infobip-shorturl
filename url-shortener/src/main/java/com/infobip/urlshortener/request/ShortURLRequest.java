package com.infobip.urlshortener.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ShortURLRequest {

    @Getter
    private String url;

    @Getter
    private int redirectType;


    public ShortURLRequest(String url){
        this.redirectType = 302;
        this.url = url;
    }

}
