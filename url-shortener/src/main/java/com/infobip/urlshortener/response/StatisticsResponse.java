package com.infobip.urlshortener.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public class StatisticsResponse {

    @Getter
    private Map<String, Integer> urlStatistics;

    public StatisticsResponse(){
        urlStatistics = new HashMap<String, Integer>();

    }

}
