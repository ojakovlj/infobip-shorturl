package com.infobip.urlshortener.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {

    @Getter
    private boolean success;

    @Getter
    private String description;

    @Getter
    private String password;


}
