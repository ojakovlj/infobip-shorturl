package com.infobip.urlshortener.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ShortURLResponse {

    @Getter
    private String shortURL;

}
