package com.infobip.urlshortener.model;

import lombok.Getter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    @Id @Getter
    private String accountId;
    @Getter
    private String password;
    @OneToMany(cascade = {CascadeType.ALL}) @Getter
    private List<ShortURL> shortURLMap;

    public User(){}

    public User(String userName, String password){
        this.accountId = userName;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Username: "+ accountId +", password: "+password;
    }
}
