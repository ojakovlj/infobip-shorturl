package com.infobip.urlshortener.model;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ShortURL {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Getter
    private String url;
    @Getter
    private String shortUrl;
    @Getter
    private int redirectType;
    public int redirects;

    public ShortURL(){}

    public ShortURL(String url, String shortUrl, int redirects, int redirectType){
        this.url = url;
        this.shortUrl = shortUrl;
        this.redirects = redirects;
        this.redirectType = redirectType;
    }

    @Override
    public String toString() {
        return "URL: "+ url +", short URL: "+shortUrl+", redirects="+redirects;
    }

}
