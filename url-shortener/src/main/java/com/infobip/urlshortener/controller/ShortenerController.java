package com.infobip.urlshortener.controller;
import com.infobip.urlshortener.model.ShortURL;
import com.infobip.urlshortener.model.User;
import com.infobip.urlshortener.repository.UserRepository;
import com.infobip.urlshortener.request.AccountRequest;
import com.infobip.urlshortener.request.ShortURLRequest;
import com.infobip.urlshortener.response.AccountResponse;
import com.infobip.urlshortener.response.ShortURLResponse;
import com.infobip.urlshortener.response.StatisticsResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import java.util.Base64;

@Configuration
@RestController
public class ShortenerController {

    private String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/account", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountResponse> account(@RequestBody AccountRequest accountRequest) {
        AccountResponse response;
        String AccountID = accountRequest.getAccountId();

        // Check username
        if(userRepository.getFirstByAccountId(AccountID) != null) {
            response = new AccountResponse(false, "Account ID already exists", "");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        else{
            // Generate password
            String password = generateRandom(15);
            response = new AccountResponse(true, "Your account is opened", password);
            // Add user to userRepository
            userRepository.save(new User(AccountID, password));

            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShortURLResponse> register(
            @RequestHeader(value = "Authorization", required=false) String credentials,
            @RequestBody ShortURLRequest request){

        if(credentials == null || !authenticateUser(credentials))
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ShortURLResponse(request.getUrl()));

        String decodedCredentials = new String(Base64.getDecoder().decode(credentials));
        // Generate short URL and store it
        String shortURL = generateRandom(8);
        User user = userRepository.getFirstByAccountId(decodedCredentials.split(":")[0]);
        user.getShortURLMap().add(new ShortURL(request.getUrl(), shortURL, 0, request.getRedirectType()));
        userRepository.save(user);

        ShortURLResponse response = new ShortURLResponse(shortURL);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistic/{AccountId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatisticsResponse> statistic(
            @RequestHeader(value = "Authorization", required=false) String credentials,
            @PathVariable String AccountId){

        StatisticsResponse response = new StatisticsResponse();

        if(credentials == null || !authenticateUser(credentials))
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);

        User user = userRepository.getFirstByAccountId(AccountId);
        for(ShortURL urlData : user.getShortURLMap()){
            response.getUrlStatistics().put(urlData.getUrl(), urlData.redirects);
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping("/help")
    public ModelAndView help () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forward:help.html");
        return modelAndView;
    }

    @RequestMapping(value = "/redirect/{ShortURL}", method = RequestMethod.GET)
    protected ModelAndView redirect(@RequestHeader(value = "Authorization", required=false) String credentials, @PathVariable String ShortURL, ModelAndView mav)
    {
        mav.setViewName("redirect:http://localhost:8080/help");
        if(credentials == null || !authenticateUser(credentials))
            return mav;

        String decodedCredentials = new String(Base64.getDecoder().decode(credentials));
        User user = userRepository.getFirstByAccountId(decodedCredentials.split(":")[0]);

        for(com.infobip.urlshortener.model.ShortURL urlData : user.getShortURLMap()){
            if(urlData.getShortUrl().equals(ShortURL)) {
                urlData.redirects++;
                userRepository.save(user);
                RedirectView red = new RedirectView(urlData.getUrl(),false);
                red.setStatusCode(urlData.getRedirectType() == 301 ?
                        HttpStatus.MOVED_PERMANENTLY : HttpStatus.MOVED_TEMPORARILY);
                System.out.println("Redirecting to "+ urlData.getUrl());
                mav.setViewName("redirect:"+urlData.getUrl());
                return mav;
            }
        }

        return mav;
    }


    /**
     * Generates and returns a random string of the given length. Uses characters defined in the
     * "characters" string.
     * @param length total length of the generated string
     * @return random string of given length
     */
    private String generateRandom(int length){
        return RandomStringUtils.random( length, characters );
    }


    /**
     * Authenticates the user credentials based on entires in the user repository.
     * @param credentials User credentials in the "accountID:password" format
     * @return true if user authentication succeeded, false otherwise
     */
    private boolean authenticateUser(String credentials){
        // Base64 decode credentials
        credentials = new String(Base64.getDecoder().decode(credentials));

        // Check credentials
        String[] userCredentials = credentials.split(":");
        if(userCredentials.length == 2) {
            String username = credentials.split(":")[0];
            String password = credentials.split(":")[1];

            User user =  userRepository.getFirstByAccountId(username);
            if(user != null && user.getPassword().equals(password))
                return true; // Authorization successful
        }

        return false;
    }
}
