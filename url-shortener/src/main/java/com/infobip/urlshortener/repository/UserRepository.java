package com.infobip.urlshortener.repository;

import com.infobip.urlshortener.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User getFirstByAccountId(String username);

    User save(User user);
}