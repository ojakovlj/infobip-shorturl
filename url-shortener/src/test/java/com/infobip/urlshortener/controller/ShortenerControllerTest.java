package com.infobip.urlshortener.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infobip.urlshortener.model.User;
import com.infobip.urlshortener.response.AccountResponse;
import com.infobip.urlshortener.WebIntegrationTestBase;
import com.infobip.urlshortener.response.ShortURLResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Base64;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

public class ShortenerControllerTest extends WebIntegrationTestBase {

	@Before
	public void setupUsers(){
		userRepository.deleteAll();
		userRepository.save(
				new User("userOne","passwordAlpha"));
		userRepository.save(
				new User("userTwo","passwordBeta"));
		userRepository.save(
				new User("userThree","passwordGamma"));
	}

	@Test
	public void testCreateAccount() throws Exception{
		String actual = mockMvc.perform(post("/account")
				.content("{\"accountId\":\"olegTestAcc1\"}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andReturn().getResponse().getContentAsString();
		System.out.println("Response: "+actual);

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		AccountResponse response = mapper.readValue(actual, AccountResponse.class);

		Assert.assertEquals(response.isSuccess(), true);
		Assert.assertEquals(response.getDescription(), "Your account is opened");
	}

	@Test
	public void testCreateExistingAccount() throws Exception{
		String actual = mockMvc.perform(post("/account")
				.content("{\"accountId\":\"userOne\"}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andReturn().getResponse().getContentAsString();
		System.out.println("Response: "+actual);

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		AccountResponse response = mapper.readValue(actual, AccountResponse.class);

		Assert.assertEquals(response.isSuccess(), false);
		Assert.assertEquals(response.getDescription(), "Account ID already exists");
	}


	@Test
	public void testRegisterURL() throws Exception{
		String credentials = Base64.getEncoder().encodeToString("userOne:passwordAlpha".getBytes());

		String url = "www.stackoverflow.com";
		String actual = mockMvc.perform(post("/register")
				.header(HttpHeaders.AUTHORIZATION,credentials)
				.content("{\"url\":\""+url+"\",\"redirectType\":302}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		System.out.println("Response: "+actual);

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		ShortURLResponse response = mapper.readValue(actual, ShortURLResponse.class);

		Assert.assertNotEquals(response.getShortURL(), url);
	}

	@Test
	public void testRegisterURLWrongCredentials() throws Exception{
		String credentials = Base64.getEncoder().encodeToString("userOne:passWordBeta".getBytes());

		String url = "www.stackoverflow.com";
		String actual = mockMvc.perform(post("/register")
				.header(HttpHeaders.AUTHORIZATION,credentials)
				.content("{\"url\":\""+url+"\",\"redirectType\":302}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andReturn().getResponse().getContentAsString();
		System.out.println("Response: "+actual);

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		ShortURLResponse response = mapper.readValue(actual, ShortURLResponse.class);

		Assert.assertNotEquals(response.getShortURL(), url, false);
	}

	@Test
	public void testUrlRedirect() throws Exception{
		String credentials = Base64.getEncoder().encodeToString("userOne:passwordAlpha".getBytes());

		String url = "www.stackoverflow.com";
		String actual = mockMvc.perform(post("/register")
				.header(HttpHeaders.AUTHORIZATION,credentials)
				.content("{\"url\":\""+url+"\",\"redirectType\":302}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		ShortURLResponse response = mapper.readValue(actual, ShortURLResponse.class);
		String shortUrl = response.getShortURL();

		// Perform redirect
		mockMvc.perform(get("/redirect/"+shortUrl)
			.header(HttpHeaders.AUTHORIZATION,credentials))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl(url));
	}


	@Test
	public void testRedirectStatistics() throws Exception{
		String credentials = Base64.getEncoder().encodeToString("userOne:passwordAlpha".getBytes());

		String url = "www.google.com";
		String actual = mockMvc.perform(post("/register")
				.header(HttpHeaders.AUTHORIZATION,credentials)
				.content("{\"url\":\""+url+"\",\"redirectType\":301}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		// Convert JSON response to object
		ObjectMapper mapper = new ObjectMapper();
		ShortURLResponse response = mapper.readValue(actual, ShortURLResponse.class);
		String shortUrl = response.getShortURL();

		testUrlRedirect();

		// Perform redirect several times
		for(int i=0; i<4; i++)
			mockMvc.perform(get("/redirect/"+shortUrl)
					.header(HttpHeaders.AUTHORIZATION,credentials))
					.andExpect(status().is3xxRedirection())
					.andExpect(redirectedUrl(url));

		actual = mockMvc.perform(get("/statistic/userTwo")
				.header(HttpHeaders.AUTHORIZATION,credentials))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		System.out.println("Response: "+actual);
	}

}