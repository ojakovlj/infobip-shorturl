package com.infobip.urlshortener;

import com.infobip.urlshortener.repository.UserRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

@Profile("test")
@RunWith(SpringRunner.class)
public class IntegrationTestBase {

    @Autowired
    protected UserRepository userRepository;

    @Before
    public void setupIntegrationTestBase() {
        userRepository.deleteAll();
    }
}