# url-shortener

Basic URL shortener built with Spring Boot with user registration and statistics tracking.

## Getting Started

* To download the project, open the Git bash in the desired target folder and run
```git clone https://ojakovlj@bitbucket.org/ojakovlj/infobip-shorturl.git```
* This project was created using JDK 1.8 and Maven 3.3.9 in IntelliJ IDEA. 
* Open your IDE of choice and import the project. Maven will download the required dependencies.
* You can build the project and run the tests by running ```mvn clean package```

### How to Run

Navigate to the folder where the .jar file is located and run it with ```
java -jar urlshortener-1.0.jar```

## Usage instructions

This section describes the requests that need to be sent to each endpoint to test the functionality. 
It assumes that the Spring application is running.

### To create an account

Send a POST request to the ```localhost:8080/account``` endpoint. JSON data should countain the following key-value pair:
```{"accountId":"myAccountName"}```

The response will contain a boolean telling whether it has succeeded or failed, a description text, and the user password.

### To register an URL to the URL shortener

Send a POST request to the ```localhost:8080/register``` endpoint. There must be an Authorization Header with the following 
format of credentials encoded in Base64: ```username:password```
The request body should be JSON data formatted as ```{"url":"MyVeryLongUrl", "redirectType": 302}```

The response will contain a shortened URL if successful or the original URL if unsuccessful.

### To use the URL shortener

Send a GET request to the ```localhost:8080/redirect/{ShortURL}``` endpoint. There must be an Authorization Header with the following 
format of credentials encoded in Base64: ```username:password```

If successful, you will be redirected to the original URL.

### To view user statistics 

Send a GET request to the ```localhost:8080/statistic/{AccountId}``` endpoint. There must be an Authorization Header with the following 
format of credentials encoded in Base64: ```username:password```

The response will be a hashmap containing the original URL and the number of redirects.

## Components

### ShortenerController.java

The REST endpoints are located in this source file. Supported endpoints:

* ```/account``` - Allows user registration by receiving an AccountId. A password is returned along
        with a description and success status
* ```/register``` - Used to register an URL. Returns a shortened URL. Requires authentication
* ```/statistics/{AccountId}``` - Allows access to user's statistics, listing all the shortened URLs and
        a number of redirects. Requires authentication
* ```/redirect/{ShortURL}``` - Redirects the user according to the shortened URL.  Requires authentication
* ```/help``` - Displays this help page

### UrlShortenerApplication.java

This is the main class for the Spring Boot application.

### ShortenerControllerTest.java

Contains the integration tests for the ShortenerController. Six included tests cover the entire 
functionality of the application's endpoints. ```testCreateAccount``` and ```testCreateExistingAccount```
are aimed at the /account endpoint, ```testRegisterURL``` and ```testRegisterURLWrongCredentials```
are aimed at the /register endpoint, ```testUrlRedirect``` checks the redirection functionality and
```testRedirectStatistics``` checks the statistics feature.

### UserRepository.java

A CRUD repository for storing User objects. Defines several methods used by the ShortenerController.

### User.java

A repository entity containing information of a user's account, including the AccountId, the password and the 
user's registered URLs.

### ShortURL.java

This class describes a URL entry submitted by a user. Contains the original full URL, the shortened
URL, the number of redirects and the redirect type (301 or 302).

### ShortURLRequest.java

POJO which serves as a JSON request for the /register endpoint. Contains the original URL which needs to be
shortened and the desired redirect type.

### AccountResponse.java

POJO created from JSON as a response for the /account endpoint. Has a boolean flag with success information,
description (success or failure) and the user's password.

### ShortURLResponse.java

POJO contains the shortened URL, a response from the /register endpoint.

### StatisticsResponse.java

POJO created from the response of the /statistics endpoint. Has a hashmap of original URLs and a redirect counter.